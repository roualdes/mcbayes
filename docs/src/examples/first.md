```@meta
EditURL = "<unknown>/docs/src/literate/first.jl"
```

# Heat Equation

a pic was here
*Figure 1*: Temperature field on the unit square with an internal uniform heat source
solved with homogeneous Dirichlet boundary conditions on the boundary.

!!! tip
    This example is also available as a Jupyter notebook:
    [`heat_equation.ipynb`](<unknown>/examples/first.ipynb).

## Introduction

The heat equation is the "Hello, world!" equation of finite elements.
Here we solve the equation on a unit square, with a uniform internal source.
The strong form of the (linear) heat equation is given by

```math
 -\nabla \cdot (k \nabla u) = f  \quad x \in \Omega,
```

where $u$ is the unknown temperature field, $k$ the heat conductivity,
$f$ the heat source and $\Omega$ the domain. For simplicity we set $f = 1$
and $k = 1$. We will consider homogeneous Dirichlet boundary conditions such that
```math
u(x) = 0 \quad x \in \partial \Omega,
```
where $\partial \Omega$ denotes the boundary of $\Omega$.

The resulting weak form is given by
```math
\int_{\Omega} \nabla v \cdot \nabla u \ d\Omega = \int_{\Omega} v \ d\Omega,
```
where $v$ is a suitable test function.

## Commented Program

Now we solve the problem in Ferrite. What follows is a program spliced with comments.
The full program, without comments, can be found in the next [section](@ref first).

First we load Ferrite, and some other packages we need

````@example first
using Statistics
````

We start  generating a simple grid with 20x20 quadrilateral elements
using `mean`.

````@example first
x = randn(10)
mean(x)
````

Here follows a version of the program without any comments.
The file is also available here: [`first.jl`](first.jl).

```julia
using Statistics

x = randn(10)
mean(x)
```

---

*This page was generated using [Literate.jl](https://github.com/fredrikekre/Literate.jl).*

