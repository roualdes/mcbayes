ADlp(method::Symbol, lp, x) = ADlp(Val{method}(), lp, x)

struct RAD{F, T, R}
    lp::F
    tape::T
    result::R
end

function RAD(lp, x)
    tape = ReverseDiff.compile(ReverseDiff.GradientTape(lp, (x,)))
    res = map(ReverseDiff.DiffResults.GradientResult, (similar(x), ))
    return RAD(lp, tape, res)
end

function val_grad!(rad::RAD, x)
    ReverseDiff.gradient!(rad.result, rad.tape, (x,))
end

function val(rad::RAD)
    return DiffResults.value(rad.result[1])
end

function grad(rad::RAD)
    return DiffResults.gradient(rad.result[1])
end

# TODO double check this works
# function val_grad_hess(rad::RAD, x, data)
#     ReverseDiff.hessian!(rad.result, rad.tape, (x,))
#     out = rad.result[1]
#     return DiffResults.value(out), DiffResults.gradient(out), DiffResults.hessian(out)
# end

ADlp(::Val{:reverse}, lp, x) = RAD(lp, x)

struct FAD{F, C, R}
    lp::F
    cfg::C
    result::R
end

function FAD(lp, x)
    cfg = ForwardDiff.GradientConfig(lp, x)
    res = DiffResults.GradientResult(x)
    return FAD(lp, cfg, res)
end

function FADh(lp, x)
    res = DiffResults.HessianResult(x)
    cfg = ForwardDiff.HessianConfig(lp, res, x)
    return FAD(lp, cfg, res)
end

function val_grad!(fad::FAD, x)
    ForwardDiff.gradient!(fad.result, fad.lp, x, fad.cfg)
end

function val_hess!(fad::FAD, x)
    ForwardDiff.hessian!(fad.result, fad.lp, x, fad.cfg)
end

function grad(fad::FAD)
    return DiffResults.gradient(fad.result)
end

function val(fad::FAD)
    return DiffResults.value(fad.result)
end

function hess(fad::FAD)
    return DiffResults.hessian(fad.result)
end

# TODO sort out when deal with hessian
# function val_grad_hess(fad::FAD, x)
#     ForwardDiff.hessian!(fad.result, fad.lp, x, fad.cgf)
#     return DiffResults.value(fad.result),
#     DiffResults.gradient(fad.result),
#     DiffResults.hessian(fad.result)
# end

ADlp(::Val{:forward}, lp, x) = FAD(lp, x)
ADlp(::Val{:forward_h}, lp, x) = FADh(lp, x)

abstract type AbstractMAD end

struct MAD{F, T <: Real} <: AbstractMAD
    lpgrad::F
    value::Vector{T}
    gradient::Vector{T}
end

struct MADH{F, T <: Real} <: AbstractMAD
    lpgrad::F
    value::Vector{T}
    gradient::Vector{T}
    hessian::Union{Matrix{T}, Diagonal{T}}
end

function MAD(lpgrad, x)
    v = Vector{eltype(x)}(undef, 1)
    out = lpgrad(x)
    l = length(out)
    if l == 2
        return MAD(lpgrad, v, similar(out[2]))
    elseif l == 3
        return MADH(lpgrad, v, similar(out[2]), similar(out[3]))
    else
        error("Function should return value and grad, or value, grad, and hessian.")
    end
end

function val_grad!(mad::MAD, x)
    lp, grad = mad.lpgrad(x)
    mad.value .= lp
    mad.gradient .= grad
end

function val_hess!(madh::MADH, x)
    lp, grad, hess = madh.lpgrad(x)
    madh.value .= lp
    madh.gradient .= grad
    madh.hessian .= hess
end

function val(mad::AbstractMAD)
    return mad.value[1]
end

function grad(mad::AbstractMAD)
    return mad.gradient
end

function hess(mad::MADH)
    return mad.hessian
end

ADlp(::Val{:manual}, lpgrad, x) = MAD(lpgrad, x)

abstract type AbstractStanAD end

mutable struct StanModelStruct
end

mutable struct StanAD <: AbstractStanAD
    lib::Ptr{Nothing}
    stanmodel::Ptr{StanModelStruct}
    D::Int
    data::String
    seed::UInt32
    logdensity::Vector{Float64}
    grad::Vector{Float64}
    function StanAD(stanlib_::Ptr{Nothing}, datafile_::String, seed_ = 204)
        seed = convert(UInt32, seed_)

        stanmodel = ccall(Libc.Libdl.dlsym(stanlib_, "create"),
                          Ptr{StanModelStruct},
                          (Cstring, UInt32),
                          datafile_, seed)

        D = ccall(Libc.Libdl.dlsym(stanlib_, "get_num_unc_params"),
                  Cint,
                  (Ptr{Cvoid},),
                  stanmodel)

        sm = new(stanlib_, stanmodel, D, datafile_, seed, zeros(1), zeros(D))

        function f(sm)
            ccall(Libc.Libdl.dlsym(sm.lib, "destroy"),
                  Cvoid,
                  (Ptr{Cvoid},),
                  sm.stanmodel)
        end

        finalizer(f, sm)
    end
end

function val_grad!(sad::StanAD, x; propto = 1, jacobian = 1)
    ccall(Libc.Libdl.dlsym(sad.lib, "log_density_gradient"),
          Cvoid,
          (Ptr{StanModelStruct},  Cint, Ref{Cdouble},
           Ref{Cdouble}, Ref{Cdouble}, Cint, Cint),
          sad.stanmodel, sad.D, x, sad.logdensity, sad.grad, propto, jacobian)
end

function val(sad::AbstractStanAD)
    return -sad.logdensity[1]
end

function grad(sad::AbstractStanAD)
    return -sad.grad
end

function destroy(sad::AbstractStanAD)
    ccall(Libc.Libdl.dlsym(sad.lib, "destroy"),
          Cvoid,
          (Ptr{StanModelStruct},),
          sad.stanmodel)
end

ADlp(::Val{:stan}, lib, datafile) = StanAD(lib, datafile)
