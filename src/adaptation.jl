function calculatewindows(warmup, initbuffer, termbuffer, windowsize)
    openwindow = initbuffer
    lastwindow = warmup - termbuffer
    closewindow = openwindow + windowsize
    nextclosewindow = closewindow + windowsize * 2
    windowsizes = [windowsize]
    closewindows = [closewindow]

    while nextclosewindow <= lastwindow
        windowsize = 2 * windowsize
        closewindow = closewindow + windowsize
        nextclosewindow = closewindow + windowsize * 2
        if nextclosewindow > lastwindow
            closewindow = lastwindow
        end

        push!(windowsizes, windowsize)
        push!(closewindows, closewindow)
    end
    return (openwindow = openwindow,
            lastwindow = lastwindow,
            closewindows = closewindows)
end

struct WindowedAdapter
    initpercent::Int
    termpercent::Int
    windowsize::Int
    openwindow::Int
    lastwindow::Int
    warmup::Int
    closewindows::Vector{Int}
end

function WindowedAdapter(warmup; initbuffer = 75, termbuffer = 50, windowsize = 25)
    windows = if warmup == 0
        println("No adaptation will occur.")
        calculatewindows(0, 0, 0, typemax(Int))
    else
        if warmup < initbuffer + termbuffer
           println("No metric adaptation will occur.")
        end
        calculatewindows(warmup, initbuffer, termbuffer, windowsize)
    end

    return WindowedAdapter(initbuffer, termbuffer, windowsize,
                              windows.openwindow, windows.lastwindow,
                              warmup, windows.closewindows)
end


# TODO adaptmetric! needs cleaning up
# could probably ditch all the chain = 1 methods in welford
# and just call chain = 1 here
function adaptmetric!(i, as, θ)
    if as.wa.openwindow <= i <= as.wa.lastwindow
        accumulatemoments!(as.ws, θ)
    end

    if i in as.wa.closewindows
        as.M .= samplevariance(as.ws)
        reset!(as.ws)
    end
end

function adaptmetric!(i, c, as, θ)
    if as.wa.openwindow <= i <= as.wa.lastwindow
        accumulatemoments!(as.ws, c, θ)
    end

    if i in as.wa.closewindows
        as.M[c, :] .=  samplevariance(as.ws, c)
        reset!(as.ws, c)
    end
end

# hopefully out of commission
# function adaptmetric!(i, as::Vector{<:AbstractSampler}, θs)
#     # TODO right now, all updates stored in as[1], then as[c].M are updated from as[1].
#     # all as[c] probably should ... I don't know
#     # TODO reuse adaptmetric(i, as:AbstractSampler, θ),
#     # instead of recreating this; ...maybe fixed the previous TODO?
#     if as[1].wa.openwindow <= i <= as[1].wa.lastwindow
#         for c in eachindex(as)
#             accumulatemoments!(as[1].ws, θs[c][as[1].idx])
#         end
#     end

#     if i in as[1].wa.closewindows
#         M = samplevariance(as[1].ws)
#         for c in eachindex(as)
#             as[c].M .= M
#         end
#         reset!(as[1].ws)
#     end
# end
