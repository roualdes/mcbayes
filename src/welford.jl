# only works for vector mass matrix
struct WelfordState{T<:AbstractFloat}
    m::VecOrMat{T}
    s::VecOrMat{T}
    n::Vector{Int}
end

function WelfordState(T, chains, d)
    return WelfordState(zeros(T, d, chains),
                        zeros(T, d, chains),
                        zeros(Int, chains))
end

WelfordState(chains, d) = WelfordState(Float64, chains, d)

WelfordState(d) = WelfordState(Float64, 1, d)

function accumulatemoments!(ws::WelfordState, chain, x)
    ws.n[chain] += 1
    d = x .- ws.m[:, chain]
    @. ws.m[:, chain] += d / ws.n[chain]
    ws.s[:, chain] .= updatecov(ws.s[:, chain], d, x .- ws.m[:, chain])
    return
end

accumulatemoments!(ws::WelfordState, x) = accumulatemoments!(ws, 1, x)

function updatecov(s::Vector, d, dp)
    return @. s + d * dp
end

function updateCov(s::Matrix, d, dp)
    return s .+ dp * d'
end

function samplevariance(ws::WelfordState, chain)
    if ws.n[chain] > 1
        σ = ws.s[:, chain] / (ws.n[chain] - 1)
        w = ws.n[chain] / (ws.n[chain] + 5)
        @. σ = w * σ + (1 - w) * 1e-3
        return σ
    end
    return ones(eltype(ws.s), length(ws.s[:, chain]))
end

samplevariance(ws::WelfordState) = samplevariance(ws, 1)

function variance(ws::WelfordState, chain)
    if ws.n[chain] > 1
        σ = ws.s[:, chain] / (ws.n[chain] - 1)
        return σ
    end
    return ones(eltype(ws.s), length(ws.s[:, chain]))
end

variance(ws::WelfordState) = variance(ws, 1)

function reset!(ws::WelfordState, chain)
    ws.n[chain] = zero(ws.n[chain])
    ws.m[:, chain] .= zero(ws.m[:, chain])
    ws.s[:, chain] .= zero(ws.s[:, chain])
    return
end

reset!(ws::WelfordState) = reset!(ws, 1)
