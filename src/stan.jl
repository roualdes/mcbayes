struct StanSampler <: AbstractSampler end

function StanHMC(initialθ, f, data;
                 AD = :forward,
                 iterations = 1000,
                 warmup = 1000,
                 D = length(initialθ),
                 M = ones(eltype(initialθ), D),
                 wa = WindowedAdapter(warmup),
                 ws = WelfordState(eltype(initialθ), 1, D),
                 rng = Random.MersenneTwister(rand(1:typemax(Int))),
                 ε = 1.0,
                 μ = log(10 * ε),
                 δ = 0.8,
                 γ = 0.05,
                 t0 = 10.0,
                 κ = 0.75,
                 maxtreedepth = 10,
                 maxdeltaH = 1000,
                 initializedraws = true,
                 findstepsize = true,
                 adaptε = true,
                 adaptM = true)

    S = eltype(initialθ)
    I = warmup + iterations

    adstan = AD == :stan
    lp = if adstan
        # ADlp(AD, f, data)
        lib = Libc.Libdl.dlopen(f)
        ADlp(AD, lib, data)
    else
        ADlp(AD, θ -> f(θ, data), initialθ)
    end

    return (sampler = StanSampler(),
            iterations,
            warmup,
            D,
            wa,
            lp,
            # TODO should probably rethink this
            # as most of these won't change mid run
            # only need to be set at initialization.
            knobs = ComponentVector{S}(ε = ε,
                                       μ = μ,
                                       δ = δ,
                                       γ = γ,
                                       κ = κ,
                                       t0 = t0,
                                       counter = 0,
                                       εbar = 0,
                                       xbar = 0,
                                       sbar = 0),
            rng,
            M,
            ws,
            maxtreedepth,
            maxdeltaH,
            initializedraws,
            findstepsize,
            adaptε,
            adaptM,
            accepted = zeros(Bool, I),
            stepsize = zeros(S, I),
            leapfrog = zeros(Int, I),
            energy = zeros(S, I),
            acceptstat = zeros(S, I),
            treedepth = zeros(S, I),
            divergence = zeros(Bool, I))
end

function _kernel!(as::StanSampler, i, stan, θs)
    info = _stankernel!(θs[1], θs[2], stan.rng, stan.D, stan.M,
                        stan.knobs.ε, stan.maxdeltaH,
                        stan.maxtreedepth, stan.lp)
    updateinfo!(i, stan, info)
    stan.adaptε && adaptstepsize!(i, stan, θs[2]) # TODO cleanup
    stan.adaptM && adaptmetric!(i, stan, θs[2])
end


function _updateinfo!(as::StanSampler, i, stan, info)
    stan.accepted[i] = info.accepted
    stan.divergence[i] = info.divergence
    stan.energy[i] = info.energy
    stan.stepsize[i] = info.stepsize
    stan.acceptstat[i] = info.acceptstat
    stan.treedepth[i] = info.treedepth
    stan.leapfrog[i] = info.leapfrog
end

function adaptstepsize!(i, stan, θ)
    if i <= stan.warmup
        dualaveragestepsize!(i, stan)

        if i in stan.wa.closewindows
            stan_findstepsize!(stan, θ)
            stan.knobs.μ = log(10 * stan.knobs.ε)
            stan.knobs.sbar = zero(stan.knobs.sbar)
            stan.knobs.xbar = zero(stan.knobs.xbar)
            stan.knobs.counter = zero(stan.knobs.counter)
        end
    else
        stan.knobs.ε = stan.knobs.εbar
    end
end

function dualaveragestepsize!(i, stan)
    counter = stan.knobs.counter
    α = stan.acceptstat[i]
    sbar = stan.knobs.sbar

    counter += 1
    α = α > 1 ? 1 : α
    eta = 1 / (counter + stan.knobs.t0)
    sbar = (1 - eta) * sbar + eta * (stan.knobs.δ - α)
    x = stan.knobs.μ - sbar * sqrt(counter) / stan.knobs.γ
    xeta = counter ^ -stan.knobs.κ
    stan.knobs.xbar = xeta * x + (1 - xeta) * stan.knobs.xbar
    stan.knobs.ε = exp(x)
    stan.knobs.εbar = exp(stan.knobs.xbar)

    stan.knobs.counter = counter
    stan.knobs.sbar = sbar
end

function stancriterion(psharp_m, psharp_p, rho)
    return dot(psharp_p, rho) > 0 && dot(psharp_m, rho) > 0
end

function _stankernel!(θ1, θ2, rng, D, M, ε, maxdeltaH,
                      maxtreedepth, lp)
    T = eltype(θ1)
    z = (q = copy(θ1), p = generatemomentum(rng, D, M))
    val_grad!(lp, z.q)
    H0 = hamiltonian(val(lp), z.p, M)

    zf = (q = copy(z.q), p = copy(z.p))
    zb = (q = copy(z.q), p = copy(z.p))
    zsample = (q = copy(z.q), p = copy(z.p))
    zpr = (q = copy(z.q), p = copy(z.p))

    # Momentum and sharp momentum at forward end of forward subtree
    pff = copy(z.p)
    psharpff = z.p .* M

    # Momentum and sharp momentum at backward end of forward subtree
    pfb = copy(z.p)
    psharpfb = copy(psharpff)

    # Momentum and sharp momentum at forward end of backward subtree
    pbf = copy(z.p)
    psharpbf = copy(psharpff)

    # Momentum and sharp momentum at backward end of backward subtree
    pbb = copy(z.p)
    psharpbb = copy(psharpff)

    # Integrated momenta along trajectory
    rho = copy(z.p)

    α = zero(T)
    lsw = zero(T)
    depth = zero(maxtreedepth)
    nleapfrog = zero(Int)

    divergence = zero(Bool)
    accepted = zero(Bool)

    while depth < maxtreedepth

        rhof = zero(rho)
        rhob = zero(rho)
        lswsubtree = typemin(T)

        if rand(rng, Bool)
            rhob .= rho
            pbf .= pff
            psharpbf .= psharpff

            z.q .= zf.q
            z.p .= zf.p

            validsubtree, nleapfrog, lswsubtree, α  =
                buildtree!(depth, z, zpr, M, rng,
                           psharpfb, psharpff, rhof, pfb, pff,
                           H0, ε, maxdeltaH, lp,
                           nleapfrog, lswsubtree, α)

            zf.q .= z.q
            zf.p .= z.p
        else
            rhof .= rho
            pfb .= pbb
            psharpfb .= psharpbb

            z.q .= zb.q
            z.p .= zb.p

            validsubtree, nleapfrog, lswsubtree, α =
                buildtree!(depth, z, zpr, M, rng,
                           psharpbf, psharpbb, rhob, pbf, pbb,
                           H0, -ε, maxdeltaH, lp,
                           nleapfrog, lswsubtree, α)

            zb.q .= z.q
            zb.p .= z.p
        end

        if !validsubtree
            divergence = true
            break
        end
        depth += one(depth)

        if lswsubtree > lsw
            zsample.q .= zpr.q
            zsample.p .= zpr.p
            accepted = true
        else
            if rand(rng, T) < exp(lswsubtree - lsw)
                zsample.q .= zpr.q
                zsample.p .= zpr.p
                accepted = true
            end
        end

        lsw = logsumexp(lsw, lswsubtree)

        # Demand satisfication around merged subtrees
        @. rho = rhob + rhof
        persist = stancriterion(psharpbb, psharpff, rho)

        # Demand satisfaction between subtrees
        rhoextended = rhob + pfb
        persist &= stancriterion(psharpbb, psharpfb, rhoextended)

        @. rhoextended = rhof + pbf
        persist &= stancriterion(psharpbf, psharpff, rhoextended)

        !persist && break
    end # end while


    θ2 .= zsample.q
    val_grad!(lp, θ2)
    return (accepted = accepted,
            divergence = divergence,
            energy = hamiltonian(val(lp), zsample.p, M),
            stepsize = ε,
            acceptstat = α / nleapfrog,
            treedepth = depth,
            leapfrog = nleapfrog)
end

function update_acceptstat(Δ)
    return Δ > zero(Δ) ? one(Δ) : exp(Δ)
end

function buildtree!(depth, z_, zpropose, M, rng,
                    psharpbeg, psharpend, rho_, pbeg, pend,
                    H0, direction, maxdeltaH, lp,
                    nleapfrog, logsumweight, α)
    T = eltype(z_.q)
    if iszero(depth)
        leapfrog!(z_.q, z_.p, direction, M, lp, grad(lp))
        nleapfrog += one(nleapfrog)
        zpropose.p .= z_.p
        zpropose.q .= z_.q

        H = hamiltonian(val(lp), z_.p, M)
        isnan(H) && (H = typemax(T))
        divergent = divergence(H, H0, maxdeltaH)

        Δ = H0 - H
        logsumweight = logsumexp(logsumweight, Δ)
        α += update_acceptstat(Δ)

        @. psharpbeg = z_.p * M
        psharpend .= psharpbeg

        rho_ .+= z_.p
        pbeg .= z_.p
        pend .= z_.p

        return !divergent, nleapfrog, logsumweight, α
    end

    lswinit = typemin(T)

    pinitend = similar(z_.p)
    psharpinitend = similar(z_.p)
    rhoinit = zero(rho_)

    validinit, nleapfrog, lswinit, α =
        buildtree!(depth - one(depth), z_, zpropose, M, rng,
                   psharpbeg, psharpinitend, rhoinit, pbeg, pinitend,
                   H0, direction, maxdeltaH, lp, nleapfrog, lswinit, α)

    if !validinit
        return validinit, nleapfrog, logsumweight, α
    end

    zfinalpr = (q = copy(z_.q), p = copy(z_.p))

    lswfinal = typemin(T)

    psharpfinalbeg = similar(z_.p)
    pfinalbeg = similar(z_.p)
    rhofinal = zero(rho_)

    validfinal, nleapfrog, lswfinal, α =
        buildtree!(depth - one(depth), z_, zfinalpr, M, rng,
                   psharpfinalbeg, psharpend, rhofinal, pfinalbeg, pend,
                   H0, direction, maxdeltaH, lp, nleapfrog, lswfinal, α)

    if !validfinal
        return validfinal, nleapfrog, logsumweight, α
    end

    lswsubtree = logsumexp(lswinit, lswfinal)

    if lswfinal > lswsubtree
        zpropose.q .= zfinalpr.q
        zpropose.p .= zfinalpr.p
    else
        if rand(rng, T) < exp(lswfinal - lswsubtree)
            zpropose.q .= zfinalpr.q
            zpropose.p .= zfinalpr.p
        end
    end

    logsumweight = logsumexp(logsumweight, lswsubtree)

    rhosubtree = rhoinit + rhofinal
    rho_ .+= rhosubtree

    # Demand satisfaction around merged subtrees
    persist = stancriterion(psharpbeg, psharpend, rhosubtree)

    # Demand satisfaction between subtrees
    @. rhosubtree = rhoinit + pfinalbeg
    persist &= stancriterion(psharpbeg, psharpfinalbeg, rhosubtree)

    @. rhosubtree = rhofinal + pinitend
    persist &= stancriterion(psharpinitend, psharpend, rhosubtree)

    return persist, nleapfrog, logsumweight, α
end

function leapfrog!(q, p, ε, M, lp, glp)
    val_grad!(lp, q)
    glp = grad(lp)
    @. p -= 0.5 * ε * glp
    @. q += ε * p * M
    val_grad!(lp, q)
    glp = grad(lp)
    @. p -= 0.5 * ε * glp
end

function stan_findstepsize!(sampler, initialθ)
    T = eltype(initialθ)
    z = (q = copy(initialθ), p = generatemomentum(sampler.rng, sampler.D, sampler.M))
    lp = sampler.lp
    val_grad!(lp, z.q)
    H0 = hamiltonian(val(lp), z.p, sampler.M)

    leapfrog!(z.q, z.p, sampler.knobs.ε, sampler.M, lp, grad(lp))
    H = hamiltonian(val(lp), z.p, sampler.M)
    isnan(H) && (H = typemax(T))

    ΔH = H0 - H
    logd8 = convert(T, log(0.8))
    direction = ΔH > logd8 ? 1 : -1

    while true
        z.q .= initialθ
        z.p .= generatemomentum(sampler.rng, sampler.D, sampler.M)
        H0 = hamiltonian(val(lp), z.p, sampler.M)

        leapfrog!(z.q, z.p, sampler.knobs.ε, sampler.M, lp, grad(lp))
        H = hamiltonian(val(lp), z.p, sampler.M)
        isnan(H) && (H = typemax(T))

        ΔH = H0 - H
        if direction == 1 && !(ΔH > logd8)
            break
        elseif direction == -1 && !(ΔH < logd8)
            break
        else
            sampler.knobs.ε = direction == 1 ? 2 * sampler.knobs.ε : 0.5 * sampler.knobs.ε
        end

        @assert sampler.knobs.ε <= 1.0e7 "Posterior is impropoer.  Please check your model."
        @assert sampler.knobs.ε >= 0.0 "No acceptable small step size could be found.  Perhaps the posterior is not continuous."
    end
end

function stan_initializedraws!(sampler, initialθ; initattempts = 100, initradius = 2)
    _stan_initializedraws!(initialθ, sampler.rng, sampler.D, sampler.lp,
                           initattempts = initattempts, initradius = initradius)
end

function _stan_initializedraws!(initialθ, rng, D, lp;
                                initattempts = 100, initradius = 2)
    initialized = false
    a = zero(initattempts)
    T = eltype(initialθ)

    while a < initattempts && !initialized
        initialθ .= generateuniform(rng, T, D, initradius)

        val_grad!(lp, initialθ)

        E = val(lp)
        if isfinite(E) && !isnan(E)
            initialized = true
        end

        g = sum(grad(lp))
        if isfinite(g) && !isnan(g)
            initialized &= true
        end

        a += one(a)
    end

    @assert a <= initattempts && initialized "Failed to find inital values in $(initattempts) attempts."
end
