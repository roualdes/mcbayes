abstract type AbstractRWMSampler <: AbstractSampler end

struct RWMSampler <: AbstractRWMSampler end
struct RWMDSampler <: AbstractRWMSampler end

function RWMD(initialθ,
              parameters;
              iterations = 2000,
              warmup = div(iterations, 2),
              idx = getindices(initialθ, parameters),
              pdim = length(idx),
              rng = Random.MersenneTwister.(rand(1:typemax(Int))))
    return (sampler = RWMDSampler(),
            parameters = parameters,
            iterations = iterations,
            warmup = warmup,
            idx = idx,
            pdim = pdim,
            accepted = zeros(Bool, iterations))
end


function RWM(initialθ,
             parameters;
             iterations = 2000,
             warmup = div(iterations, 2),
             idx = getindices(initialθ, parameters),
             pdim = length(idx),
             ε = 2.38 / sqrt(length(idx)), # TODO 1.0? just scales M, so why? quicker adaption?
             M = ones(pdim),
             ws = WelfordState(eltype(initialθ), 1, pdim),
             rng = Random.MersenneTwister.(rand(1:typemax(Int))))
    return (sampler = RWMSampler(),
            parameters = parameters,
            iterations = iterations,
            warmup = warmup,
            idx = idx,
            pdim = pdim,
            M = M,
            accepted = zeros(Bool, iterations))
end



function _kernel!(as::AbstractRWMSampler, i, rwm, θs, lp;
                  data = nothing, adaptmetric = true, kwargs...)
    info = _rwmkernel!(rwm, θs[i-1], θs[i], rwm.idx, rwm.rng, rwm.pdim, rwm.M, rwm.ε, lp, data)
    updateinfo!(i, rwm, info)
    # TODO adaptation for RWMD
    adaptmetric && as isa RWMSampler && adaptmetric!(i, rwm, θs[i]) # adaptation within a chain
end

function _rwmkernel!(rwm, θ1, θ2, idx, rng, pdim, M, ε, lp, data)
    θ2[idx] .= generateproposal(rwm.sampler, old, idx, rng, pdim, M, ε)
    a = lp(θ2, data) - lp(old, data)
    accepted = log(rand(rng)) < a
    @inbounds θ2[idx] .= @view(θ2[idx]) * accepted + @view(old[idx]) * (1 - accepted)
    return (accepted = accepted,)
end

function generateproposal(rwmd::RWMDSampler, θold, idx, rng, pdim, M, ε)
    return θold[idx] .+ rand(rng, (-1, 1), pdim)
end

function generateproposal(rwm::RWMSampler, θold, idx, rng, pdim, M, ε)
    return θold[idx] .+ ε .* randn(rng, pdim) .* sqrt.(M)
end

function _updateinfo!(as::AbstractRWMSampler, i, rwm, info)
    rwm.accepted[i] = info.accepted
end
