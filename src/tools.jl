function preallocatedraws(θ, C)
    return [[similar(θ) for _ in 1:2] for _ in 1:C]
end

function preallocateDF(θ, N, C)
    T = eltype(θ)
    D = length(θ)
    df = hcat(
        DataFrame(chain = repeat(1:C, inner = N),
                  iteration = repeat(1:N, outer = C)),
        DataFrame(reduce(hcat, [Vector{Float64}(undef, N * C) for _ in 1:D]),
                  indexparameters(θ))
    )
    return df
end

function indexparameters(θ::ComponentVector)
    parameters = Vector{String}(undef, length(θ))
    b = 1
    for k in keys(θ)
        p = θ[k]
        l = length(p)
        if l > 1
            parameters[b:b+l-1] .= string(k) .* "[" .* string.(1:l) .* "]"
        else
            parameters[b] = string(k)
        end
        b += l
    end
    return parameters
end

function indexparameters(θ::Vector)
    return "θ[" .* string.(1:length(θ)) .* "]"
end

# TODO do I want 50:50 continuous:discrete sampling back?
function getindices(θ, syms::Tuple{Vararg{Symbol}})
    idx = Int[]
    for s in syms
        append!(idx, label2index(θ, string(s)))
    end
    return idx
end


# iteration tools
function thin!(t, krnl, θs, lp, gradlp = nothing; data = nothing)
    # TODO only last info captured
    # Unsure of how to do better without complicated return structures
    tmp = similar(θs[1])
    tmp .= θs[1]
    local info
    for _ in 1:t
        info = kernel!(krnl, [tmp, θs[2]], lp, gradlp, data = data)
        tmp .= θs[2]
    end
    return info
end


function log1pexp(a)
    a > zero(a) && return a + log1p(exp(-a))
    return log1p(exp(a))
end


function logsumexp(a, b)
    a, b = promote(a, b)
    T = typeof(a)
    a == typemin(T) && return b
    isinf(a) && isinf(b) && return typemax(T)
    a > b && return a + log1pexp(b - a)
    return b + log1pexp(a - b)
end

function logsumexp(v::Vector)
    T = eltype(v)
    length(v) == 0 && return typemin(T)
    m = maximum(v)
    isinf(m) && return m
    return m + log(sum(vi -> exp(vi - m), v))
end

function logit(p)
    return log(p / (1 - p))
end

function invlogit(x)
    return 1 / (1 + exp(-x))
end

function logmix(λ, p1, p2)
    return logsumexp(log(λ) + p1, log1p(-λ) + p2)
end

function softmax(v::Vector)
    length(v) == 0 && return v
    m = maximum(v)
    theta = exp.(v .- m)
    return theta ./ sum(theta)
end

function logsoftmax(v::Vector)
    return v .- logsumexp(v)
end


function rand_gumbel(N::Int)
    return -log.(-log.(rand(N)))
end

function rand_onehotlogits_index(logits::Vector)
    # adapted from
    # https://en.wikipedia.org/wiki/Categorical_distribution
    l = length(logits)
    g = rand_gumbel(l)
    return argmax(g .+ logits)
end

function rand_onehotlogits(logits::Vector)
    onehot = zeros(length(logits))
    onehot[rand_onehotlogits_index(logits)] = 1
    return onehot
end

function rand_onehotprobs(probs::Vector)
    return rand_onehotlogits(log.(probs))
end


# function binomci(x, n; p = 0.5, alpha = 0.05)
#     BL = Beta(x, n - x + 1)
#     BU = Beta(x + 1, n - x)
#     a2 = alpha / 2
#     l = quantile(BL, a2)
#     u = quantile(BU, 1 - a2)
#     # Assuming a CLT
#     # third value is true if algorithm successful
#     return l, u, l <= p <= u
# end

function sum_groups!(out, xs, idx_map; groups = nothing)
    # TODO sorting (something) first might be faster
    # than length(groups) sweeps through xs
    groups !== nothing && (groups = unique(idx_map))
    for (k, u) in pairs(groups)
        out[k] = sum(@view xs[idx_map .== u])
    end
end

function sum_groups(xs, idx_map)
    groups = unique(idx_map)
    out = zeros(eltype(xs), length(groups))
    sum_groups!(out, xs, idx_map, groups = groups)
    return out
end

function onehot(x)
    N = length(x)
    groups = unique(x)
    K = length(groups)
    Z = zeros(N, K)
    for i in 1:N
        idx = findfirst(isequal(x[i]), groups)
        Z[i, idx] = 1
    end
    return Z
end

function onehotsparse(x)
    N = length(x)
    groups = unique(x)
    K = length(groups)
    rows = []
    cols = []
    for i in 1:N
        idx = findfirst(isequal(x[i]), groups)
        push!(rows, i)
        push!(cols, idx)
    end
    return SparseArrays.sparse(rows, cols, ones(length(rows)))
end

function harmonicmean(x)
    return inv(mean(inv, x))
end

function halton(n::Int; base::Int = 2)
    x = 0.0
    s = 1.0
    while n > 0
        s /= base
        n,r = divrem(n, base)
        x += s*r
    end
    return x
end

function generateuniform(rng, T, N, radius)
    return radius .* (2 .* rand(rng, T, N) .- 1)
end

function generateuniform(T, N, radius)
    return generateuniform(Random.default_rng(), T, N, radius)
end


function generatemomentum(rng, dim, M::Matrix{T}) where {T}
    cholesky(Symmetric(M)).U \ randn(rng, T, dim)
end

function generatemomentum(rng, dim, M::Vector{T}) where {T}
    z = randn(rng, T, dim)
    return z ./ sqrt.(M)
end

function generatemomentum(rng, dim, M::Vector{T}, go) where {T}
    z = randn(rng, T, dim)
    p = 1 ./ (1 .+ exp.(go .* z))
    z .*= 2 .* (rand(rng, dim) .< p) .- 1
    return z ./ sqrt.(M)
end

function generatemomentum(dim, M)
    return generatemomentum(Random.default_rng(), dim, M)
end

function hamiltonian(lp, p, M)
    return lp + p' * Diagonal(M) * p * oftype(lp, 0.5)
end

function hamiltonian(lp, p)
    return lp + p' * p * oftype(lp, 0.5)
end

function divergence(H, H0, limit)
    return H - H0 > limit
end


function _csum(f, x, mx = zero(x))
    s = zero(eltype(f(first(x))))
    @inbounds @simd for n in eachindex(x, mx)
        s += f(x[n] - mx[n])
    end
    return s
end

function _cdot(x, mx, y, my = zero(y))
    s = zero(eltype(dot(first(x), first(y))))
    @inbounds @simd for n in eachindex(x, y)
        s += (x[n] - mx[n]) * (y[n] - my[n])
    end
    return s
end

function _cdotsq(x, mx, y, my = zero(y))
    s = _cdot(x, mx, y, my)
    return s * s
end

function wmean(x, w)
    T = eltype(x)
    a = zero(T)
    xw = zero(T)
    for i in eachindex(x, w)
        a += w[i]
        xw += w[i] * (x[i] - xw) / a
    end
    return xw
end

function table(x::Vector)
    d = Dict{eltype(x), Int}()
    for v in x
        idx = Base.ht_keyindex2!(d, v)
        if idx > 0
            @inbounds d.vals[idx] += 1
        else
            @inbounds Base._setindex!(d, 1, v, -idx)
        end
    end
    return d
end

function group(x::AbstractVector, g::AbstractVector)
    gt = eltype(g)
    gt <: Number && gt != Int && (g = convert.(Int, g))
    t = table(g)
    d = Dict(k => Vector{eltype(x)}(undef, v) for (k, v) in t)
    ug = collect(keys(t))
    u = gt <: Number ? ug : Dict(k => i for (i, k) in pairs(ug))
    c = zeros(Int, length(d))
    @inbounds for i in eachindex(g, x)
        gi = g[i]
        ui = u[gi]
        c[ui] += 1
        d[gi][c[ui]] = x[i]
    end
    return d
end

function gmap(f, x, g::AbstractVector; sortfirst = false)
    v = collect(group(x, g))
    z = sortfirst ? sort(v, by = x -> first(x)) : v
    return map(y -> f(last(y)), z)
end

# Could calc z before hand
# z = sort(collect(MCBayes.group(C, data.id)), by = x -> first(x))
# function gmap2(f, x, z)
#     return map(y -> f(last(y)), z)
# end



function coordinate_draws!(draws::Vector, i)
    draws[i] .= draws[i-1]
end

function coordinate_draws!(draws::Vector{Vector{T}}, i) where T
    @inbounds for c in eachindex(draws)
        draws[c][i] .= draws[c][i-1]
    end
end


# linear form
function lf!(C, X, β, Z::T, b) where {T <:AbstractMatrix}
    mul!(C, X, β)
    mul!(C, Z, b, true, true)
end


function lf!(C, X, β, id::T, b) where {T <: AbstractVector}
    @inbounds for (n, i) in pairs(id)
        C[n] = b[i]
    end
    mul!(C, X, β, true, true)
end

function lf(X, β, zid, b)
    C = Vector{eltype(X)}(undef, size(X, 1))
    lf!(C, X, β, zid, b)
    return C
end
