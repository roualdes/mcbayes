# Adapted from https://github.com/gzanella/barker
# https://github.com/gzanella/barker/blob/master/functions.R
struct BarkerSampler <: AbstractSampler end

function Barker(initialθ,
                parameters;
                iterations = 2000,
                chains = 4,
                warmup = div(iterations, 2),
                idx = getindices(initialθ, parameters),
                pdim = length(idx),
                M = ones(pdim, chains),
                ε = 2.4 / sqrt(length(idx) ^ (1/3)),
                δ = 0.4,
                rng = Random.Xoshiro.(rand(1:typemax(Int), chains)))
    S = eltype(initialθ)
    μ = ones(S, pdim, chains)
    for c in 1:chains
        μ[:, c] .= zeros(pdim) # initialθ
    end
    return (sampler = BarkerSampler(),
            parameters = parameters,
            iterations = iterations,
            chains = chains,
            warmup = warmup,
            idx = idx,
            pdim = pdim,
            μ = μ,
            εs = ones(S, chains) .* ε,
            M = M,
            δ = δ,
            rng = rng,
            accepted = zeros(Bool, chains, iterations),
            acceptstat = zeros(S, chains, iterations),
            stepsize = zeros(S, iterations))
end

function _kernel!(as::BarkerSampler, i, bkr, θs, lpg;
                  data = nothing, adaptmetric = true,
                  adaptstepsize = true, kwargs...)
    idx = bkr.idx
    γ = i ^ -0.6

    @inbounds for c in 1:bkr.chains
        old = θs[c][i-1]
        new = θs[c][i]

        z = randn(bkr.rng[c], bkr.pdim) .* sqrt.(bkr.M[:, c]) .* bkr.εs[c]

        lpo, go = lpg(old, data)
        lpo *= -1

        p = 1 ./ (1 .+ exp.(go .* z))
        b = 2 .* (rand(bkr.rng[c], bkr.pdim) .< p) .- 1

        new[idx] .= old[idx] .+ b .* z
        lpn, gn = lpg(new, data)
        lpn *= -1

        βn = gn .* (old[idx] .- new[idx])
        βo = go .* (new[idx] .- old[idx])

        log_ratio = lpn - lpo
        a = 0
        if log_ratio > -300     # github_ref#L144
            a = min(1, exp(log_ratio + sum(logsumexp.(0, βo) .- logsumexp.(0, βn))))
        end
        accepted = rand(bkr.rng[c]) < a

        @views new[idx] .= new[idx] .* accepted .+ old[idx] .* (1 - accepted)
        updateinfo!(i, c, bkr, (accepted = accepted, acceptstat = a, stepsize = bkr.εs[c]))

        # adaptations
        log_sigma_2 = log(bkr.εs[c] ^ 2) + γ * (a - bkr.δ)
        bkr.εs[c] = sqrt(exp(log_sigma_2))

        @. bkr.μ[:, c] += γ * (new[idx] - bkr.μ[:, c])
        @. bkr.M[:, c] += γ * ((new[idx] - bkr.μ[:, c]) ^ 2 - bkr.M[:, c])
    end
end

function _updateinfo!(as::BarkerSampler, i, c, bkr, info)
    bkr.accepted[c, i] = info.accepted
    bkr.acceptstat[c, i] = info.acceptstat + 1e-20
    bkr.stepsize[i] = info.stepsize
end
