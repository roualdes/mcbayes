# Adapted from
# [1] https://arxiv.org/pdf/2110.11576.pdf
# [2] https://proceedings.mlr.press/v130/hoffman21a.html
# [3] https://github.com/tensorflow/probability/blob/c678caa1b8e94ab3677a37e581e8b19e68e59248/tensorflow_probability/python/experimental/mcmc/gradient_based_trajectory_length_adaptation.py
# [4] https://github.com/tensorflow/probability/blob/5ebcdf1f32ecc340dece4f21694790ea95c6c1e2/spinoffs/fun_mc/fun_mc/sga_hmc.py
# [5] https://github.com/tensorflow/probability/blob/5ebcdf1f32ecc340dece4f21694790ea95c6c1e2/spinoffs/fun_mc/fun_mc/fun_mc_lib.py

abstract type AbstractSGASampler <: AbstractSampler end

function SGA(method::Symbol, initialθ, f, data = nothing;
             D = length(initialθ),
             S = eltype(initialθ),
             iterations = 1000,
             warmup = iterations,
             chains = 10,
             M = ones(S, D),
             AD = :forward,
             jitter = :uniform,
             ε = 0.5,
             T = ε,
             δ = 0.8,
             α = 0.05,
             β1 = 0.0,
             β2 = 0.5,
             τ = 1e-8,
             γ = -0.6,
             maxleapfrogsteps = 1000,
             maxdeltaH = 1000,
             adaptε = true,
             adaptM = true,
             adaptr = true,
             adaptT = true,
             adaptchains = true,
             nt = Threads.nthreads(),
             kwargs...)

    # TODO add some checks for length match nt: data, rng
    datas = Tuple(deepcopy(data) for _ in 1:nt)
    rng = Tuple(Random.Xoshiro(rand(UInt32)) for _ in 1:chains)

    adstan = in(AD, (:stan, :stanms))
    lp = if adstan
        Tuple(ADlp(AD, f, initialθ) for n in 1:nt)
    else
        Tuple(ADlp(AD, θ -> f(θ, datas[n]), initialθ) for n in 1:nt)
    end
    if !adstan
        val_grad!(lp[1], initialθ)
    end

    iterationsT = typeof(iterations)
    resetε = typemax(iterationsT)
    resetM = typemax(iterationsT)
    resetr = typemax(iterationsT)
    resetT = typemax(iterationsT)

    knobs = ComponentVector{S}(ε = ε, εbar = ε, T = T, Tbar = T)
    mε = zeros(1)
    vε = zeros(1)
    mT = zeros(1)
    vT = zeros(1)
    rm = RunningMoments(S, D)
    ghats = Vector{S}(undef, chains)
    p = zeros(S, D, chains)
    q = zeros(S, D, chains)

    I = warmup + iterations
    accepted = zeros(Bool, chains, I)
    stepsize = zeros(S, I)
    leapfrog = zeros(Int, I)
    acceptstat = zeros(S, chains, I)
    maxtrajectorylength = zeros(S, I)
    divergence = zeros(Bool, chains, I)
    return SGA(Val{method}();
               initialθ,
               f,
               AD,
               D,
               iterations,
               warmup,
               chains,
               jitter,
               nt,
               datas,
               rng,
               lp,
               M,
               ε,
               T,
               δ,
               α,
               β1,
               β2,
               τ,
               γ,
               maxleapfrogsteps,
               maxdeltaH,
               adaptε,
               adaptM,
               adaptr,
               adaptT,
               adaptchains,
               resetε,
               resetM,
               resetr,
               resetT,
               knobs,
               mε,
               vε,
               mT,
               vT,
               rm,
               ghats,
               accepted,
               stepsize,
               leapfrog,
               acceptstat,
               maxtrajectorylength,
               divergence,
               p,
               q,
               kwargs...)
end

function _kernel!(as::AbstractSGASampler, i, sga, θs)
    u = halton(i)
    j = sga.jitter == :uniform ? 2u * sga.knobs.T : -log(u) * sga.knobs.T
    L = max(1, ceil(Int, j / sga.knobs.ε))
    nt = sga.nt
    @sync for it in 1:nt
        Threads.@spawn for c in it:nt:sga.chains
            info = hmc!(i, θs[c][1], θs[c][2], sga.rng[c], sga.D, sga.M,
                        sga.knobs.ε, L, sga.maxdeltaH, sga.lp[it], sga.datas[it])
            updateinfo!(i, c, sga, (info..., maxtrajectorylength = sga.knobs.T))
        end
    end
    sga.adaptchains && adaptchains!(i, sga, θs)
end

function hmc!(i, θ1, θ2, rng, D, M, ε, L, maxdeltaH, lp, data)
    T = eltype(θ1)
    onehalf = convert(T, 0.5)
    q = copy(θ1)

    val_grad!(lp, q)
    glp = grad(lp)

    m2 = M ./ maximum(M)
    p = randn(rng, T, D) ./ sqrt.(m2)

    H1 = val(lp) + onehalf * (p' * Diagonal(m2) * p)
    isnan(H1) && (H1 = typemax(T))

    # Adapted from MCMC using Hamiltonian dynamics, Radford Neal
    # TODO cite better
    @. p -= onehalf * ε * glp

    @inbounds for l in 1:L
        @. q += ε * p * m2
        val_grad!(lp, q)
        glp .= grad(lp)         # TODO(ear) does this need copy (.=), or can it just assign since the memory is already stored within lp?
        if l != L
            @. p -= ε * glp
        end
    end

    @. p -= onehalf * ε * glp

    H2 = val(lp) + onehalf * (p' * Diagonal(m2) * p)
    isnan(H2) && (H2 = typemax(T))
    divergent = divergence(H2, H1, maxdeltaH)

    α = min(1, exp(H1 - H2))
    accepted = rand(rng, T) < α
    @. θ2 = q * accepted + θ1 * (1 - accepted)

    return (accepted = accepted,
            stepsize = ε,
            acceptstat = α,
            q = q,
            p = p,
            leapfrog = L,
            divergence = divergent)
end

function _updateinfo!(as::AbstractSGASampler, i, c, sga, info)
    sga.accepted[c, i] = info.accepted
    sga.stepsize[i] = info.stepsize
    sga.leapfrog[i] = info.leapfrog
    acceptstat = isnan(info.acceptstat) ? zero(info.acceptstat) : info.acceptstat
    sga.acceptstat[c, i] = 1e-20 + acceptstat
    sga.maxtrajectorylength[i] = info.maxtrajectorylength
    sga.divergence[c, i] = info.divergence
    sga.q[:, c] .= info.q
    sga.p[:, c] .= info.p
end

function _adaptchains!(as::AbstractSGASampler, i, sga, θs)
    if i <= sga.warmup
        αs = @view sga.acceptstat[:, i]
        αbar = harmonicmean(αs)

        sga.adaptε && _update_stepsize!(sga.sampler, i, sga, αbar)

        if i > 100 && sga.adaptT
            _update_trajectorylength!(i, sga, θs, αs, αbar)
        end

        # update_stepsize!(sga, i, αbar)
        # update_moments!(sga, i, θs)
        # update_metric!(sga, i, θs)
        # udpate_pca!(sga, i, θs)

        accumulatemoments!(sga.rm, 2, θs) # TODO clean this up
        # _update_moments!() in MCBayes.jl
        # dispatches to accumulatemoments(stan) -> Welford
        # accumulatemoments!(sga) -> RunningMoments
        sga.adaptM && _update_metric!(sga.sampler, i, sga, θs)
        sga.adaptr && _update_pca!(sga.sampler, i, sga, θs)
        # update_pca!(i, sga, θs) -> _update_pca!(sga.sampler, i, sga, θs)
        # and remove underscore _xxx from each of the above


        i == sga.resetM && reset!(sga.rm)
        i == sga.resetT && (sga.knobs.T = 0)
        i == sga.resetε && (sga.knobs.ε = 0)

        if i == sga.warmup
            sga.knobs.ε = sga.knobs.εbar
            sga.knobs.T = sga.knobs.Tbar
        end
    end
end

function _update_metric!(as::AbstractSGASampler, i, sga, θs)
    η = i ^ sga.γ               # ημ = 1 / (ceil(numchains * i / sga.κ) + 1)
    V = sga.rm.v * sga.rm.n[1] / (sga.rm.n[1] - 1)
    sga.M .= η .* V .+ (1 - η) .* sga.M
end

_update_pca!(as::T, i, sga, θs) where {T <: AbstractSGASampler} = Returns(nothing)

function _update_trajectorylength!(i, sga, θs, αs, αbar)
    _gradient!(i, sga, θs, αs)
    ghats = sga.ghats

    # [3]#L733
    αbar < 1e-4 && (ghats .= zero(ghats))
    !all(isfinite.(ghats)) && (ghats .= zero(ghats))

    ghat = wmean(ghats, αs)
    as = _adam_step!(i, ghat, sga.mT, sga.vT, sga.α, sga.β1, sga.β2, sga.τ)[1]

    logupdate = clamp(as, -0.35, 0.35) # [3]#L759
    T = sga.knobs.T * exp(logupdate) # [3]#L761
    T = clamp(T, 0, sga.knobs.ε * sga.maxleapfrogsteps) # [3]#L773

    sga.knobs.T = T
    #aw = (i - 100) ^ -0.5 # [3]#L765, -100? to account for delayed updates on T, see [1]
    aw = i ^ sga.γ
    sga.knobs.Tbar = exp(aw * log(T) + (1 - aw) * log(1e-10 + sga.knobs.Tbar))
end

function _gradient!(i, sga, θs, αs)
    D = sga.D
    numchains = sga.chains

    mθ = zeros(D)
    mq = zeros(D)
    mw = sga.rm.n[1] / (sga.rm.n[1] + numchains)
    v = zero(eltype(sga.q[1]))

    @inbounds for c in 1:numchains
        @. mθ += (θs[c][1] - mθ) / c # i-1
        v += αs[c]
        @. mq += αs[c] * (sga.q[c] - mq) / v
    end

    @. mθ = mw * sga.rm.m + (1 - mw) * mθ
    @. mq = mw * sga.rm.m + (1 - mw) * mq

    _sampler_gradient!(sga.sampler, i, sga, θs, mθ, mq)
end


function _adam_step!(t, g, m, v, α, β1, β2, τ)
    @. m = β1 * m + (1 - β1) * g
    @. v = β2 * v + (1 - β2) * g ^ 2
    a = α * sqrt(1 - β2 ^ t) / (1 - β1 ^ t)
    return @. a * m / (sqrt(v) + τ)
end

function _update_stepsize!(as::AbstractSGASampler, i, sga, αbar)
    δ = sga.δ
    m = sga.mε
    v = sga.vε
    a = sga.α
    b1 = sga.β1
    b2 = sga.β2
    τ = sga.τ
    e = sga.knobs.ε
    ebar = sga.knobs.εbar

    as = _adam_step!(i, αbar - δ, m, v, a, b1, b2, τ)[1]
    e *= exp(as)

    aw = i ^ sga.γ               # aw = i ^ -0.5 # [1]
    ebar = exp(aw * log(e) + (1 - aw) * log(1e-10 + ebar))

    sga.knobs.ε = e
    sga.knobs.εbar = ebar
end
