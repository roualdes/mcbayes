abstract type AbstractChEESSampler <: AbstractSGASampler end
struct ChEESHMCSampler <: AbstractChEESSampler end

function SGA(::Val{:chees}; kwargs...)
    return (sampler = ChEESHMCSampler(), kwargs...)
end

function _sampler_gradient!(as::AbstractChEESSampler, i, sga, θs, mθ, mq)
    t = sga.knobs.T             # TODO + sga.knobs.ε ?
    h = halton(i)
    q = Vector{eltype(θs[1][1])}(undef, sga.D)
    @inbounds @simd for c in 1:sga.chains
        q .= sga.q[:, c]
        dsq = _csum(abs2, q, mq) - _csum(abs2, θs[c][1], mθ)
        sga.ghats[c] = h * t * dsq * _cdot(q, mq, sga.p[:, c]) # TODO missing a 2 * h...?
    end
end

function cheeshmc_findstepsize!(chees, draws, lpgrad, data)
    # from [2] Section 3.1
    # For each target distribution and algorithm, we ran 100 parallel
    # chains with step size tuned by dual averaging (Nesterov, 2009;
    # Hoffman & Gelman, 2014) to achieve a harmonic-mean acceptance
    # rate of 0.651.  Initial step sizes were chosen by repeatedly
    # halving the step size (starting from a consistently too-large
    # value of 1.0) until an HMC proposal with a single leapfrog step
    # achieved a harmonic-mean acceptance probability of at least 0.5.

    T = eltype(draws[1][1])
    chains = chees.chains
    αs = zeros(T, chains)
    ε = 2.0
    hm = zero(T)
    tmp = copy(draws[2][1])
    md = length(size(chees.M)) > 1

    while hm < 0.5
        ε /= 2
        for c in 1:chains
            chees.knobs.ε = ε
            info = hmc!(0, draws[1][c], tmp,
                        chees.idx, chees.rng[c],
                        chees.pdim, md ? chees.M[:, c] : chees.M,
                        ε, 1, chees.maxdeltaH, lpgrad, data)
            αs[c] = info.acceptstat
        end
        hm = harmonicmean(αs)
    end

    chees.knobs.T = ε
    chees.knobs.Tbar = ε
    return
end
