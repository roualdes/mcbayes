module MCBayes

using ComponentArrays
using LinearAlgebra
using SparseArrays
using SpecialFunctions
using Statistics
using FFTW
using DataFrames
using RecursiveArrayTools
using ForwardDiff
using ReverseDiff
using DiffResults
using Random

abstract type AbstractSampler end

include("welford.jl")
include("runningmoments.jl")
include("adaptation.jl")
include("tools.jl")
include("densities.jl")
include("ad.jl")

include("gwg.jl")
include("rwm.jl")
include("slice.jl")
include("stan.jl")
#include("sgahmc.jl")
include("sga.jl")
include("snaper.jl")
include("chees.jl")
include("cheesr.jl")
include("monge.jl")
include("monge_static.jl")
include("barker.jl")
include("malt.jl")
include("meads.jl")
include("drhmc.jl")

include("convergence.jl")

function transition!(i, as::T, θs) where {T}
    _kernel!(as.sampler, i, as, θs)
end

updateinfo!(i, c, as::T, info)  where {T} = _updateinfo!(as.sampler, i, c, as, info)
updateinfo!(i, as::T, info)  where {T} = _updateinfo!(as.sampler, i, as, info)

adaptchains!(i, as::T, θs) where {T} = _adaptchains!(as.sampler, i, as, θs)


export
    # general
    preallocatedraws,
    preallocateDF,
    transition!,
    _kernel!,
    updateinfo!,
    coordinate_draws!,
    extract,
    removewarmup,
    onehot,
    onehotsparse,
    logit,
    invlogit,
    thin!,
    logmix,
    lf!,
    lf,
    wmean,

    # convergence
    ess_bulk,
    ess_tail,
    rhat,
    summarize,

    # rwm
    RWM,
    RWMInfo,
    RWMD,
    RWMDInfo,

    # slice
    Slice,
    # SliceInfo,
    SliceD,
    # SliceDInfo

    # gwg
    GWGBinary,

    # stan
    StanHMC,
    stan_initializedraws!,
    stan_findstepsize!,

    # sgahmc
    ChEESHMCSampler,
    ChEESRHMCSampler,
    SNAPERHMCSampler,
    SGA,
    adaptchains!,
    cheeshmc_findstepsize!,

    # barker,
    Barker,

    # meads,
    AbstractMEADSSampler,
    MEADS,
    adam_initialize!,

    # delayed rejection hmc
    DelayedRejectionMEADSSampler,
    DRHMC,

    # adaptation
    WindowedAdapter,
    adaptmetric!,
    adaptstepsize!,

    # ad
    ADlp,
    FAD,
    FADh,
    MAD,
    MADH,
    RAD,
    StanAD,
    val_grad!,
    val_hess!,
    val,
    grad,
    hess,
    destroy,

    # densities
    normal,
    grad_normal,
    mvnormal,
    grad_mvnormal,
    halfnormal,
    grad_halfnormal,
    gamma,
    grad_gamma,
    invgamma,
    grad_invgamma,
    poisson,
    grad_poisson,
    # binomial,
    grad_binomial,
    beta,
    grad_beta,
    bernoulli,
    grad_bernoulli,
    bernoulli_logit,
    grad_bernoulli_logit,
    studentt,
    grad_studentt,
    halfstudentt,
    grad_halfstudentt
end
