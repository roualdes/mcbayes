abstract type AbstractSNAPERSampler <: AbstractSGASampler end
struct SNAPERHMCSampler <: AbstractSNAPERSampler end

function SGA(::Val{:snaper}; kwargs...)
    r = randn(kwargs[:D])                # pca first axis
    r ./= norm(r)
    return (sampler = SNAPERHMCSampler(), r, kwargs...)
end

function _update_pca!(as::AbstractSNAPERSampler, i, sga, θs)
    T = eltype(θs[1][1])
    s = sga.r ./ (1e-20 + norm(sga.r))
    tmp = Vector{T}(undef, sga.D)
    numchains = sga.chains
    m = mean(c -> θs[c][2], 1:numchains)
    @inbounds for c in 1:numchains
        tmp .= θs[c][2] .- m
        s .+= tmp .* dot(tmp, s)
        s ./= 1e-20 + norm(s)
    end

    η = i ^ sga.γ               # η = sga.ηr / i # [1]
    @. sga.r = η * s + (1 - η) * sga.r
    sga.r ./= 1e-20 + norm(sga.r)
end

function _sampler_gradient!(as::AbstractSNAPERSampler, i, sga, θs, mθ, mq)
    t = sga.knobs.T + sga.knobs.ε # [4]#L643
    h = halton(i)
    r = sga.r
    @inbounds @simd for c in 1:sga.chains
        tmp = _cdot(sga.q[:, c], mq, r)
        dsq = tmp ^ 2 - _cdot(θs[c][1], mθ, r) ^ 2 # i-1, but why not 2?
        sga.ghats[c] = 4dsq * tmp * dot(sga.p[:, c], r) - dsq ^ 2 / t
        sga.ghats[c] *= h
    end
end
