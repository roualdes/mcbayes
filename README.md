# MCBayes

A Julia package for experimenting with various MCMC algorithms.

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://roualdes.gitlab.io/MCBayes.jl/)
[![Build Status](https://gitlab.com/roualdes/MCBayes.jl/badges/main/pipeline.svg)](https://gitlab.com/roualdes/MCBayes.jl/pipelines)
[![Coverage](https://gitlab.com/roualdes/MCBayes.jl/badges/main/coverage.svg)](https://gitlab.com/roualdes/MCBayes.jl/commits/master)
