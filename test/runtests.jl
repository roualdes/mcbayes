using MCBayes
using Test
using Distributions
using LinearAlgebra
using ComponentArrays
using RecursiveArrayTools
using UnPack
using Random
using DataFrames
using RCall
using JSON

import ForwardDiff

# TODO probably need to organize as
# https://discourse.julialang.org/t/nesting-testset-good-idea-or-not/2164/2
# and the links within

function gaussian(θ, data)
    @unpack l, s = data
    return mvnormal(θ, l, s)
end

function lp_gaussian(θ, data)
    @unpack x = θ
    @unpack l, s = data
    return mvnormal(x, l, s)
end

function lpg_gaussian(θ, data)
    @unpack x = θ
    @unpack l, s, glp = data
    glp.x .= first(grad_mvnormal(x, l, s))
    return mvnormal(x, l, s), getdata(glp)
end

function lpgh_gaussian(θ, data)
    @unpack H = data
    @unpack s = data
    lp, grad = lpg_gaussian(θ, data)
    H .= -s
    return lp, grad, H
end

function rand_gaussian(rng, d)
    initialdraw = ComponentVector(x = randn(rng, d))
    data = (l = zeros(d),
            s = initialdraw * initialdraw' + I,
            inv = zeros(d),
            H = Matrix(Diagonal(ones(d))),
            glp = similar(initialdraw))
    return initialdraw, data
end

@testset "generalized AD" begin

    rng = Random.Xoshiro(204)
    d = 3
    initialdraw, data = rand_gaussian(rng, d)

    flp = ADlp(:forward, θ -> lp_gaussian(θ, data), initialdraw)
    val_grad!(flp, initialdraw)

    @test isapprox(val(flp), -3.1792917742079942)
    @test isapprox(grad(flp), [-0.12398668576400115, -0.3263608976499478, -0.09551828635445675])

    q = getdata(initialdraw)
    rlp = ADlp(:reverse, θ -> gaussian(θ, data), q)
    val_grad!(rlp, q)

    @test isapprox(val(rlp), -3.1792917742079942)
    @test isapprox(grad(rlp), [-0.12398668576400115, -0.3263608976499478, -0.09551828635445675])

    mlp = ADlp(:manual, θ -> lpg_gaussian(θ, data), initialdraw)
    val_grad!(mlp, initialdraw)

    @test isapprox(val(mlp), -3.1792917742079942)
    @test isapprox(grad(mlp), [-0.12398668576400115, -0.3263608976499478, -0.09551828635445675])
end



@testset "velocity half-step" begin
    d = 3
    α = 0.5
    ε = 0.1
    rng = Random.Xoshiro(204)
    initialdraw, data = rand_gaussian(rng, d)
    lp, g, h = lpgh_gaussian(initialdraw, data)

    Nl = norm(g) ^ 2
    Lα = 1 + α ^ 2 * Nl
    sL = sqrt(Lα)
    g ./= sL
    h ./= sL

    velocity = MCBayes.generatevelocity(rng, g, Nl, α, d)
    logdet = MCBayes.velocityhalfstep!(velocity, g, h, Lα, α, 0.5 * ε, data)

    @test isapprox(velocity, [1.5548092256919772, -0.2496006676648631, 0.94433591228092])
    @test isapprox(logdet, -0.033358185781416)
end


@testset "weighted mean" begin

    x = randn(5)
    w = [0.2; 0.1; 0.15; 0.4; 0.15]

    wx = MCBayes.wmean(x, w)
    @test isapprox(wx, sum(w .* x ./ sum(w)))
end


@testset "running moments" begin
    # Adapted from
    # https://github.com/tensorflow/probability/blob/76ff71ba27a5a035fa6220e6132744ac89a56fdf/spinoffs/fun_mc/fun_mc/fun_mc_test.py#L1390
    # Copyright 2021 The TensorFlow Probability Authors.
    # Licensed under the Apache License, Version 2.0
    D = 3
    rm = MCBayes.RunningMoments(D)

    @testset "one chain" begin
        N = 100
        C = 1

        x = [[ComponentVector(x = randn(D)) for _ in 1:N] for _ in 1:C]

        for n in 1:N
            MCBayes.accumulatemoments!(rm, n, x)
        end

        y = VectorOfArray(x)
        m = mean(y)
        v = var(y, corrected = false)

        @test isapprox(m, rm.m)
        @test isapprox(v, rm.v)
        @test N == rm.n[1]
        @test isapprox(v * N / (N - 1), rm.v * rm.n[1] / (rm.n[1] - 1))
    end

    MCBayes.reset!(rm)

    @testset "reset running moments" begin
        @test all(iszero.(rm.m))
        @test all(iszero.(rm.v))
        @test all(iszero.(rm.n))
    end

    @testset "average over chains" begin
        N = 100
        C = 10

        x = [[ComponentVector(x = randn(D)) for _ in 1:N] for _ in 1:C]

        for n in 1:N
            MCBayes.accumulatemoments!(rm, n, x)
        end

        y = VectorOfArray(x)
        m = mean(y)
        v = var(y, corrected = false)

        @test isapprox(m, rm.m)
        @test isapprox(v, rm.v)
        @test N * C == rm.n[1]
        @test isapprox(v * N * C / (N * C - 1), rm.v * rm.n[1] / (rm.n[1] - 1))
    end
end


@testset "welford" begin
    D = 3
    C = 5
    W = MCBayes.WelfordState(C, D)

    N = 1000
    x = randn(N, D, C)

    for n in 1:N
        Threads.@threads for c in 1:C
            MCBayes.accumulatemoments!(W, c, x[n, :, c])
        end
    end

    @test all(W.n .== N)

    me = 4 / sqrt(N)
    @test all(-me .< W.m .< me)

    S = std(x, dims = 1)[1, :, :]
    @test isapprox(sqrt.(W.s ./ (N - 1)), S)

    rS = reduce(hcat, [MCBayes.samplevariance(W, c) for c in 1:C])
    T = @. S ^ 2 * (N / (N+5)) + (5 / (N+5)) * 1e-3
    @test isapprox(rS, T)

    foreach(c -> MCBayes.reset!(W, c), 1:C)
    @test all(W.n .== 0)
    @test all(W.m .== 0)
    @test all(W.s .== 0)
end


@testset "densities" begin
    d = ForwardDiff.derivative
    g = ForwardDiff.gradient

    # Normal
    function DN(y, l, s)
        N = Normal(l, s)
        return logpdf(N, y)
    end

    gn = grad_normal(3.0, 5, 10)

    @test normal(3.0, 5, 10) ≈ DN(3, 5, 10)
    @test (x -> d(t -> DN(t, 5, 10), x))(3) ≈ gn[1]
    @test (x -> d(t -> DN(3, t, 10), x))(5) ≈ gn[2]
    @test (x -> d(t -> DN(3, 5, t), x))(10) ≈ gn[3]

    # MVNormal
    function DMVN(y, l, s)
        N = MvNormal(l, s)
        return logpdf(N, y)
    end

    y = randn(3)
    l = [1.0; 2.0; 3.0]
    s = let x = randn(3); x * x' + Diagonal(ones(3)) end
    gmvn = grad_mvnormal(y, l, s)

    # TODO add missing terms ... somewhere
    # @test mvnormal(y, l, s) ≈ DMVN(y, l, s)
    # @test all((x -> g(t -> DMVN(t, l, s), x))(y) .≈ gmvn[1])
    # @test all((x -> g(t -> DMVN(y, t, s), x))(l) .≈ gmvn[2])
    # @test (x -> d(t -> DN(3, 5, t), x))(10) ≈ gn[3] # TODO fix in densities.jl


    # Half Normal
    function DTN(y, s)
        TN = truncated(Normal(0, s), 0, Inf)
        return logpdf.(TN, y)
    end

    gtn = grad_halfnormal(3.0, 10)

    @test halfnormal(3.0, 10) ≈ DTN(3, 10)
    @test (x -> d(t -> DTN(t, 10), x))(3) ≈ gtn[1]
    # TODO why is their's nan
    # @test (x -> d(t -> DTN(3, t), x))(10) ≈ gtn[2]


    # Student-t
    # TODO add tests that actually use location l and scale s
    function DST(y, l, s, v)
        T = TDist(v)
        return logpdf(T, y)
    end

    gst = grad_studentt(5.0, 0, 1, 3)

    @test studentt(5.0, 0, 1, 3) ≈ DST(5, 0, 1, 3)
    @test (x -> d(t -> DST(t, 0, 1, 3), x))(5) ≈ gst[1]
    @test (x -> d(t -> DST(5, 0, 1, t), x))(3) ≈ gst[4]


    # Half Student-t
    function DTST(y, s, v)
        T = truncated(TDist(v), 0, Inf)
        return logpdf(T, y)
    end

    gtst = grad_halfstudentt(5.0, 1, 3)

    @test halfstudentt(5.0, 1, 3) ≈ DTST(5, 1, 3)
    @test (x -> d(t -> DTST(t, 1, 3), x))(5) ≈ gtst[1]
    # @test (x -> d(t -> DTST(5, 1, t), x))(3.0) # errors


    # Gamma
    function DG(y, a, b)
        G = Gamma(a, 1/b)
        return logpdf(G, y)
    end

    gg = grad_gamma(0.5, 4.25, 6.75)

    @test gamma(0.5, 4.25, 6.75) ≈ DG(0.5, 4.25, 6.75)
    @test (x -> d(t -> DG(t, 4.25, 6.75), x))(0.5) ≈ gg[1]
    @test (x -> d(t -> DG(0.5, t, 6.75), x))(4.25) ≈ gg[2]
    @test (x -> d(t -> DG(0.5, 4.25, t), x))(6.75) ≈ gg[3]

    # Inverse Gamma
    function DIG(y, a, b)
        IG = InverseGamma(a, b)
        return logpdf(IG, y)
    end

    gg = grad_invgamma(0.5, 4.25, 6.75)

    @test invgamma(0.5, 4.25, 6.75) ≈ DIG(0.5, 4.25, 6.75)
    @test (x -> d(t -> DIG(t, 4.25, 6.75), x))(0.5) ≈ gg[1]
    @test (x -> d(t -> DIG(0.5, t, 6.75), x))(4.25) ≈ gg[2]
    @test (x -> d(t -> DIG(0.5, 4.25, t), x))(6.75) ≈ gg[3]

    # Poisson
    function DP(y, l)
        P = Poisson(l)
        return logpdf(P, y)
    end

    gp = grad_poisson(10, 5.5)

    @test poisson(10, 5.5) ≈ DP(10, 5.5)
    # @test (x -> d(t -> DP(t, 5.5), x))(10) ≈ gp[1] # discrete
    @test (x -> d(t -> DP(10, t), x))(5.5) ≈ gp[2]


    # Binomial
    function DBi(y, k, p)
        B = Binomial(k, p)
        return logpdf(B, y)
    end

    gbi = grad_binomial(10, 20, pi/4)

    @test MCBayes.binomial(10, 20, pi/4) ≈ DBi(10, 20, pi/4)
    # @test (x -> d(t -> DBi(t, 20, pi/4), x))(10) ≈ gbi[1] # discrete
    # @test (x -> d(t -> DBi(10, t, pi/4), x))(20) ≈ gbi[2] # errors
    @test (x -> d(t -> DBi(10, 20, t), x))(pi/4) ≈ gbi[3]


    # Beta
    function DBeta(y, a, b)
        B = Beta(a, b)
        return logpdf(B, y)
    end

    gbeta = grad_beta(0.25, 5, 7)

    @test beta(0.25, 5, 7) ≈ DBeta(0.25, 5, 7)
    @test (x -> d(t -> DBeta(t, 5, 7), x))(0.25) ≈ gbeta[1]
    @test (x -> d(t -> DBeta(0.25, t, 7), x))(5) ≈ gbeta[2]
    @test (x -> d(t -> DBeta(0.25, 5, t), x))(7) ≈ gbeta[3]


    # Bernoulli
    function DBe(y, p)
        B = Binomial(1, p)
        return logpdf(B, y)
    end

    gbe = grad_bernoulli(1, pi/4)

    @test bernoulli(1, pi/4) ≈ DBe(1, pi/4)
    # @test (x -> d(t -> DBe(t, pi/4), x))(1) ≈ gbe[1] # discrete
    @test (x -> d(t -> DBe(1, t), x))(pi/4) ≈ gbe[2]


    # Bernoulli_logit
    @test bernoulli_logit(1, 1.75) ≈ bernoulli(1, invlogit(1.75))
    @test bernoulli_logit(0, 1.75) ≈ bernoulli(0, invlogit(1.75))

    gbl = grad_bernoulli_logit(0, 1.75)
    @test (x -> d(t -> bernoulli(t, invlogit(1.75)), x))(0) ≈ gbl[1]
    @test (x -> d(t -> bernoulli(0, invlogit(t)), x))(1.75) ≈ gbl[2]

end

@testset "tools" begin

    idx = MCBayes.indexparameters(ComponentVector(x = rand(10)))
    @test all(idx .== "x[" .* string.(1:10) .* "]")

    cv = ComponentVector(a = rand(3), b = rand(2), c = rand(1), d = rand())
    @test all(MCBayes.indexparameters(cv) .== ["a[1]", "a[2]", "a[3]", "b[1]", "b[2]", "c", "d"])

end


@testset "estimate max eigenvalue" begin

    N = 200
    D = 100

    mvn = MvNormal(zeros(D), Diagonal(ones(D)))
    X = rand(mvn, N)'
    m = mean(X, dims = 1)
    sd = std(X, dims = 1)
    XC = (X .- m) ./ sd
    @test abs(MCBayes.maxeigenvalue(XC) - 1) < abs(maximum(eigvals(XC * XC' / 200)) - 1)

    mvn = MvNormal(zeros(D), Diagonal(10 .^ range(log10(0.1), log10(1), D)))
    X = rand(mvn, N)'
    m = mean(X, dims = 1)
    sd = std(X, dims = 1)
    XC = (X .- m) ./ sd
    @test abs(MCBayes.maxeigenvalue(XC) - 1) < abs(maximum(eigvals(XC * XC' / 200)) - 1)

    mvn = MvNormal(zeros(D), Diagonal(range(0.1, 1, D)))
    X = rand(mvn, N)'
    m = mean(X, dims = 1)
    sd = std(X, dims = 1)
    XC = (X .- m) ./ sd
    @test abs(MCBayes.maxeigenvalue(XC) - 1) < abs(maximum(eigvals(XC * XC' / 200)) - 1)

    mvn = MvNormal(zeros(D), Diagonal(repeat([0.1, 1], inner=50)))
    X = rand(mvn, N)'
    m = mean(X, dims = 1)
    sd = std(X, dims = 1)
    XC = (X .- m) ./ sd
    @test abs(MCBayes.maxeigenvalue(XC) - 1) < abs(maximum(eigvals(XC * XC' / 200)) - 1)

end

@testset "convergence diagnostics" begin

    rdraws = R"""
    my_pdb <- posteriordb::pdb_github()
    po <- posteriordb::posterior("eight_schools-eight_schools_centered", my_pdb)
    rpd <- posteriordb::reference_posterior_draws(po)
    rpd
    """

    draws = rcopy(rdraws)

    rsmry = R"posterior::summarize_draws(rpd)"
    smry = rcopy(rsmry)

    df = flatten(DataFrame(draws), All());
    chains = length(draws);
    iterations = 1000;
    df[:, :iteration] .= repeat(1:iterations, outer = chains);
    df[:, :chain] .= repeat(1:chains, inner = iterations);

    sdf = summarize(df, digits = 10, warmup = 0);

    @test isapprox(smry[:, :mean], sdf[:, :mean])
    @test isapprox(smry[:, :median], sdf[:, :median])
    @test isapprox(smry[:, :sd], sdf[:, :std])
    @test isapprox(smry[:, :mad], sdf[:, :mad])
    @test isapprox(smry[:, :rhat], sdf[:, :rhat])
    @test isapprox(smry[:, :ess_bulk], sdf[:, :ess_bulk])
    @test isapprox(smry[:, :ess_tail], sdf[:, :ess_tail])

end
